import pandas as pd
import urllib
from urllib.request import ProxyHandler, build_opener
import requests
from bs4 import BeautifulSoup
from parse_csv import parse_csv

file_name = 'files_xlsx/8.12_58_brown_NEW.xlsx'
parsed_file_name = 'files_xlsx/parsed/8.12_58_brown_NEW_parsed.xlsx'
jpg_directory = 'folders_jpg/8.12_58_brown_NEW_jpg/'
jpg_alt_1_list = []
jpg_alt_2_list = []
jpg_alt_3_list = []
jpg_alt_4_list = []
jpg_alt_5_list = []
product_number_list = []
description_list = []
price_list = []
prod_id_current_list = []


def get_lists_from_excel():
    df = pd.read_excel(file_name)
    return df['URL'], df['Product ID']


def get_jpg(url, prod_id):
    for i in range(1, 6):
        try:
            content_length = urllib.request.urlopen(url.replace('main', f'alt_{i}')).info()['Content-Length']
        except:
            jpg_alt_1_list.append('url error')
            jpg_alt_2_list.append('url error')
            jpg_alt_3_list.append('url error')
            jpg_alt_4_list.append('url error')
            jpg_alt_5_list.append('url error')
            return
        if content_length != '9679':
            urllib.request.urlretrieve(url.replace('main', f'alt_{i}'), f'{jpg_directory}{prod_id}_alt_{i}.jpg')
            if i == 1:
                jpg_alt_1_list.append(f'{prod_id}_alt_{i}.jpg')
            elif i == 2:
                jpg_alt_2_list.append(f'{prod_id}_alt_{i}.jpg')
            elif i == 3:
                jpg_alt_3_list.append(f'{prod_id}_alt_{i}.jpg')
            elif i == 4:
                jpg_alt_4_list.append(f'{prod_id}_alt_{i}.jpg')
            elif i == 5:
                jpg_alt_5_list.append(f'{prod_id}_alt_{i}.jpg')
        if i == 1 and content_length == '9679':
            jpg_alt_1_list.append('No alt image')
            jpg_alt_2_list.append('No alt image')
            jpg_alt_3_list.append('No alt image')
            jpg_alt_4_list.append('No alt image')
            jpg_alt_5_list.append('No alt image')
            return
        elif i == 2 and content_length == '9679':
            jpg_alt_2_list.append('No alt image')
            jpg_alt_3_list.append('No alt image')
            jpg_alt_4_list.append('No alt image')
            jpg_alt_5_list.append('No alt image')
            return
        elif i == 3 and content_length == '9679':
            jpg_alt_3_list.append('No alt image')
            jpg_alt_4_list.append('No alt image')
            jpg_alt_5_list.append('No alt image')
            return
        elif i == 4 and content_length == '9679':
            jpg_alt_4_list.append('No alt image')
            jpg_alt_5_list.append('No alt image')
            return
        elif i == 5 and content_length == '9679':
            jpg_alt_5_list.append('No alt image')
            return
    # jpg_total_list.append(jpg_current_prod_list)


def get_error(text):
    description_list.append(text)
    price_list.append(text)
    jpg_alt_1_list.append(text)
    jpg_alt_2_list.append(text)
    jpg_alt_3_list.append(text)
    jpg_alt_4_list.append(text)
    jpg_alt_5_list.append(text)


def parse_html():
    count = 0
    prev_product_number = ''
    prev_price = ''
    prev_description = ''
    url_list, prod_id_total_list = get_lists_from_excel()
    print(url_list)
    for url in url_list:
        if type(url) != str:
            continue
        name_from_url = url.split('/')[8]
        product_number = name_from_url.split('_')[0][2:]
        product_number_list.append(product_number)
        if product_number == prev_product_number:
            price_list.append(prev_price)
            description_list.append(prev_description)
            get_jpg(url, prod_id_total_list[count])
            prod_id_current_list.append(prod_id_total_list[count])
            prev_product_number = product_number
            print(count)
            count += 1
            continue
        prev_product_number = product_number
        try:
            response = requests.get(f'https://www.zulily.com/p/{product_number}')
        except:
            text = 'url error'
            get_error(text)
            prev_price = text
            prev_description = text
            prod_id_current_list.append(prod_id_total_list[count])
            print(count)
            count += 1
            continue
        html = response.text
        soup = BeautifulSoup(html, 'html.parser')
        # soup = BeautifulSoup(html)
        error_container = soup.find('div', {'id': 'error-container'})
        if error_container is not None:
            text = 'Page not found'
            get_error(text)
            prev_price = text
            prev_description = text
        else:
            # main_price = soup.find('span', {'class': 'js-pdp-main-price'})
            main_price = None
            description = soup.find('div', {'class': 'description'})
            if main_price is not None and description is not None:
                price_text = main_price.text
                descr_text = description.find('p').text + '\n\n' + get_li_rows(description.find_all('li'))
                price_list.append(price_text)
                description_list.append(descr_text)
                prev_price = price_text
                prev_description = descr_text
            elif main_price is not None and description is None:
                price_text = main_price.text
                descr_text = 'No description'
                price_list.append(price_text)
                description_list.append(descr_text)
                prev_price = price_text
                prev_description = descr_text
            elif main_price is None and description is not None:
                price_text = 'No price'
                descr_text = description.find('p').text + '\n\n' + get_li_rows(description.find_all('li'))
                price_list.append(price_text)
                description_list.append(descr_text)
                prev_price = price_text
                prev_description = descr_text
            elif main_price is None and description is None:
                price_text = 'No price'
                descr_text = 'No description'
                price_list.append(price_text)
                description_list.append(descr_text)
                prev_price = price_text
                prev_description = descr_text
            get_jpg(url, prod_id_total_list[count])
        prod_id_current_list.append(prod_id_total_list[count])
        # if count == 6:
        #     break
        count += 1
        print(count)
    df = pd.DataFrame({
        'product_number': prod_id_current_list,
        'description': description_list,
        'price': price_list,
        'jpg_alt_1_list': jpg_alt_1_list,
        'jpg_alt_2_list': jpg_alt_2_list,
        'jpg_alt_3_list': jpg_alt_3_list,
        'jpg_alt_4_list': jpg_alt_4_list,
        'jpg_alt_5_list': jpg_alt_5_list
    })
    writer = pd.ExcelWriter(parsed_file_name, engine='xlsxwriter')
    df.to_excel(writer, 'Sheet1')
    writer.save()
    print('done')
    return


def get_li_rows(li_list):
    text = ''
    for li in li_list:
        text += li.text + '\n'
    return text


if __name__ == '__main__':
    parse_html()
    # parse_csv()
