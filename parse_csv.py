import pandas as pd

file_name_csv = 'files_csv/08.12.csv'
file_name_xlsx = 'files_xlsx/parsed/08.12_shoes_58_brown_parsed.xlsx'
file_csv_parsed = 'files_csv/auto_parse/08.12_shoes_58_brown_auto_parse.csv'


def parse_csv():
    df_csv = pd.read_csv(file_name_csv, delimiter=';', header=0)
    print(df_csv)
    df_excel = pd.read_excel(file_name_xlsx)
    df_excel_jpg = df_excel.loc[
        (df_excel['jpg_alt_1_list'] != 'No alt image') &
        (df_excel['jpg_alt_1_list'] != 'Page not found') &
        (df_excel['jpg_alt_1_list'] != 'url error')
    ]
    df_csv_jpg = df_csv.loc[df_csv['sku'].isin(df_excel_jpg['product_number'].values)]
    count_jpg_dict = {}
    for index, row_excel_jpg in df_excel_jpg.iterrows():
        count_jpg = 0
        for i in range(1, 6):
            if row_excel_jpg[f'jpg_alt_{i}_list'] != 'No alt image':
                count_jpg += 1
        count_jpg_dict.update({row_excel_jpg['product_number']: count_jpg})
    name_list_csv = []
    sku_list_csv = []
    product_id_list_csv = []
    gallery_image_list_csv = []
    for index, row_csv_jpg in df_csv_jpg.iterrows():
        count = 1
        for i in range(1, count_jpg_dict[row_csv_jpg['sku']] + 1):
            product_id_list_csv.append(row_csv_jpg['product_id'])
            sku = row_csv_jpg['sku']
            gallery_image_list_csv.append(f'{sku}_alt_{i}.jpg')
            if count == 1:
                name_list_csv.append(row_csv_jpg['name'])
                sku_list_csv.append(row_csv_jpg['sku'])
            else:
                name_list_csv.append('')
                sku_list_csv.append('')
            count += 1
    df = pd.DataFrame({
        'name': name_list_csv,
        'sku': sku_list_csv,
        'product_id': product_id_list_csv,
        'gallery_image': gallery_image_list_csv
    })
    df.to_csv(file_csv_parsed, sep='\t', index=None)
    print('done')
